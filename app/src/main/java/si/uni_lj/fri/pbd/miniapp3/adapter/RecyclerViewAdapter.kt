package si.uni_lj.fri.pbd.miniapp3.adapter

import android.app.Service
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity

class RecyclerViewAdapter(private val data : List<RecipeDTO>, private val itemClickListener: OnItemClickListener) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    // ViewHolder class for this adapter
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // the views that it needs to store
        val textView: TextView
        val imageView: ImageView
        val v : View
        init {
            textView = view.findViewById(R.id.text_view_content)
            imageView = view.findViewById(R.id.image_view)
            v = view
        }
        // bind the onclick listener to the viewHolder, so onItemClicked in fragments can be called
        fun bind(id : String, click : OnItemClickListener) {
            imageView.setOnClickListener {
                click.onItemClicked(id)
            }
        }
    }
    // Item click listener for recyclerView elements to open DetailsActivity
    interface OnItemClickListener {
        fun onItemClicked(id : String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_grid_item, parent, false)
        return ViewHolder(v)
    }
    // When viewHolder is binded, set the data to display in the view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = data[position].strDrink
        Glide.with(holder.v)
            .load(data[position].strDrinkThumb)
            .into(holder.imageView)
        holder.bind(data[position].idDrink!!, itemClickListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}