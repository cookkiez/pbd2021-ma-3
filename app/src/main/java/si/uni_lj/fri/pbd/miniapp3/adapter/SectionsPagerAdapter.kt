package si.uni_lj.fri.pbd.miniapp3.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesFragment
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SectionsPagerAdapter.newInstance] factory method to
 * create an instance of this fragment.
 */
class SectionsPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when(position) {
            0 -> return SearchFragment()
            1 -> return FavoritesFragment()
        }
        return SearchFragment()
    }
}
