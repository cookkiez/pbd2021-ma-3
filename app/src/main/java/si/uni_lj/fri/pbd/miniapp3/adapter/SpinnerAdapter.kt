package si.uni_lj.fri.pbd.miniapp3.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO

class SpinnerAdapter(private val data : MutableList<String>) : BaseAdapter() {
    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return 0L
    }

    // Inflate the view and set the text of current item
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.spinner_item, parent, false)
        v.findViewById<TextView>(R.id.text_view_spinner_item).text = data[position]
        return v
    }
}