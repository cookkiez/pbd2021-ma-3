package si.uni_lj.fri.pbd.miniapp3.database

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

class Repo(application: Application?) {
    val searchResults = MutableLiveData<List<RecipeDetails>>()
    var allRecipes: LiveData<List<RecipeDetails>>

    private val recipeDao : RecipeDao

    // Insert recipe into DB
    fun insertRecipe(newRecipe: RecipeDetails) {
        Database.databaseWriteExecutor.execute(Runnable {
            recipeDao.insertRecipe(newRecipe) })
    }
    // Delete recipe from DB
    fun deleteRecipe(name: String) {
        Database.databaseWriteExecutor.execute(Runnable {
            recipeDao.deleteRecipe(name) })
    }
    // find the recipe in the DB
    fun findRecipe(name: String) {
        Database.databaseWriteExecutor.execute(Runnable {
            searchResults.postValue(recipeDao.findRecipe(name))
        })
    }

    fun getAllRecipes(){
        allRecipes = recipeDao.allRecipes
    }

    init {
        val db: Database =
            Database.getDatabase(application?.applicationContext!!)!!
        recipeDao = db.recipeDao()
        allRecipes = recipeDao.allRecipes
    }
}