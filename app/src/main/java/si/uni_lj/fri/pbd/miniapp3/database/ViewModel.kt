package si.uni_lj.fri.pbd.miniapp3.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

class ViewModel(application: Application?) : AndroidViewModel(application!!){
    var allRecipes: LiveData<List<RecipeDetails>>
    var searchResults: MutableLiveData<List<RecipeDetails>>

    private val repository: Repo

    // Intermediary functions for inserting, deleting and finding recipes in DB
    fun insertProduct(recipe: RecipeDetails) {
        repository.insertRecipe(recipe)
    }
    fun findProduct(name: String) {
        repository.findRecipe(name)
    }
    fun deleteProduct(name: String) {
        repository.deleteRecipe(name)
    }
    fun getAllRecipes(){
        repository.getAllRecipes()
        allRecipes = repository.allRecipes
    }

    init {
        repository = Repo(application)
        allRecipes = repository.allRecipes
        searchResults = repository.searchResults
    }
}