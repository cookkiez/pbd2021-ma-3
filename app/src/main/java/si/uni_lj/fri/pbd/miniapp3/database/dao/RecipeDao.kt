package si.uni_lj.fri.pbd.miniapp3.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails

@Dao
interface RecipeDao {

    // Functions for getting, storing and deleting data from and into the DB
    @Query("SELECT * FROM recipeDetails WHERE idDrink = :idDrink")
    fun getRecipeById(idDrink: String?): RecipeDetails?

    @get:Query("SELECT * FROM recipeDetails")
    val allRecipes: LiveData<List<RecipeDetails>>

    @Query("DELETE FROM recipeDetails WHERE strDrink = :name")
    fun deleteRecipe(name: String)

    @Query("SELECT * FROM recipeDetails WHERE strDrink = :name")
    fun findRecipe(name: String): List<RecipeDetails>

    @Insert
    fun insertRecipe(recipe: RecipeDetails)

    // TODO: Add the missing methods
}