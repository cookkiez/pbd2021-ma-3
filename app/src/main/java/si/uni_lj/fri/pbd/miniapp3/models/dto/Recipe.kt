package si.uni_lj.fri.pbd.miniapp3.models.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Recipe {
    @SerializedName("drinks")
    @Expose
    val recipe: List<RecipeDetailsDTO>? = null
}