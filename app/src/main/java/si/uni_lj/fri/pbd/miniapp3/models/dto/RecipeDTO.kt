package si.uni_lj.fri.pbd.miniapp3.models.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RecipeDTO{
    @SerializedName("strDrink")
    @Expose
    var strDrink:String? = null

    @SerializedName("strDrinkThumb")
    @Expose
    var strDrinkThumb:String? = null

    @SerializedName("idDrink")
    @Expose
    var idDrink:String? = null
}