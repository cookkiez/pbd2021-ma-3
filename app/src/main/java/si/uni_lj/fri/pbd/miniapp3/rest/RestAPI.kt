package si.uni_lj.fri.pbd.miniapp3.rest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import si.uni_lj.fri.pbd.miniapp3.models.dto.*

interface RestAPI {
    @get:GET("list.php?i=list")
    val allIngredients: Call<IngredientsDTO?>?

    // Get recipes by ingredient
    @GET("filter.php")
    fun recipesByIngredientDTO(@Query("i") ingredient:String?): Call<RecipesDTO>

    // Get recipe details by id
    @GET("lookup.php")
    fun recipeDetails(@Query("i") id:String?): Call<Recipe>

    // Get all non alcoholic drinks
    @get:GET("filter.php?a=Non_Alcoholic")
    val nonAlcoholic : Call<RecipesDTO>

}