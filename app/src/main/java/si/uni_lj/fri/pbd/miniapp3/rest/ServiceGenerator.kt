package si.uni_lj.fri.pbd.miniapp3.rest

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import si.uni_lj.fri.pbd.miniapp3.Constants
import timber.log.Timber
import java.util.*


object ServiceGenerator {
    private var sBuilder: Retrofit.Builder? = null
    private var sHttpClient: OkHttpClient.Builder? = null
    private var sRetrofit: Retrofit? = null
    private const val baseUrl: String = "https://www.thecocktaildb.com/api/json/v1/1/"
    fun init() {
        sHttpClient = OkHttpClient.Builder()
        sBuilder = Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()) // TODO: add converter

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        sHttpClient!!.addInterceptor(interceptor)
        sRetrofit = sBuilder?.client(sHttpClient?.build())?.build()
        Timber.d("Retrofit built with base url: %s", sRetrofit?.baseUrl()?.toUrl().toString())
    }

    fun <S> createService(serviceClass: Class<S>?): S {
        return sRetrofit!!.create(serviceClass)
    }

    init {
        init()
    }
}