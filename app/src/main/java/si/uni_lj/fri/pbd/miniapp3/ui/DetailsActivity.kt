package si.uni_lj.fri.pbd.miniapp3.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.database.ViewModel
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.dto.Recipe

class DetailsActivity : AppCompatActivity() {
    var id = ""
    var thisRecipe : RecipeDetailsDTO = RecipeDetailsDTO()
    private var mViewModel: ViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        mViewModel = ViewModel(application)

        //Get id of the drink
        id = intent.getStringExtra("id")!!

        //Check from which fragment this activity was created and act accordingly
        val fragment = intent.getStringExtra("fragment")!!
        val fav = findViewById<Button>(R.id.favoriteButton)
        if (fragment == "favorites") {
            fav.text = "Unfavorite"
        } else {
            fav.text = "Favorite"
        }

        //init stuff
        val service = ServiceGenerator.createService(RestAPI::class.java)
        val call = service.recipeDetails(id)
        val instr = findViewById<TextView>(R.id.instructions)
        val n = findViewById<TextView>(R.id.name)
        val ingr = findViewById<TextView>(R.id.ingredients)
        val meas = findViewById<TextView>(R.id.measures)
        val img = findViewById<ImageView>(R.id.image_view)
        val c = this
        // Get the drink from the api and set the view items correctly
        call.enqueue(object : Callback<Recipe> {
            override fun onResponse(
                call: Call<Recipe>,
                response: Response<Recipe>
            ) {
                var r = response.body()!!
                val recipe = r.recipe?.get(0)
                thisRecipe = recipe!!
                instr.text = "Instructions:\n" + recipe.strInstructions
                Glide.with(c)
                    .load(recipe.strDrinkThumb)
                    .into(img)
                n.text = recipe.strDrink
                ingr.text = "Ingredients:\n" + recipe.strIngredient1 + ", " + recipe.strIngredient2 +
                        ", " + recipe.strIngredient3
                meas.text = "Measures:\n" + recipe.strMeasure1 + ", " + recipe.strMeasure2 + ", " + recipe.strMeasure3

                // If this activity was created from the favorites fragment set button to delete,
                // otherwise set to store recipe in database
                fav.setOnClickListener {
                    if (fragment == "favorites") {
                        mViewModel!!.deleteProduct(thisRecipe.strDrink!!)
                        finish()
                    } else {
                        mViewModel!!.insertProduct(Mapper.mapRecipeDetailsDtoToRecipeDetails(true, thisRecipe))
                    }
                }
            }

            override fun onFailure(call: Call<Recipe>, t: Throwable) {
                Log.d("Fail", "FAIL")
            }
        })

    }
}