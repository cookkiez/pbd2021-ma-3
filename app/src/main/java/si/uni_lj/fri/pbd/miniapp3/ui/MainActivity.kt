package si.uni_lj.fri.pbd.miniapp3.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayoutMediator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.SectionsPagerAdapter
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesFragment
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager : ViewPager2

    companion object{
        var semesterMode = false
        var nonAlcoholic = mutableListOf<String>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val service = ServiceGenerator.createService(RestAPI::class.java)
        val nonAlcoholCall = service.nonAlcoholic
        nonAlcoholCall.enqueue(object : Callback<RecipesDTO> {
            override fun onResponse(call: Call<RecipesDTO>, response: Response<RecipesDTO>) {
                val recipes = response.body()
                nonAlcoholic.clear()
                for(rec in recipes!!.recipes!!) {
                    nonAlcoholic.add(rec.idDrink!!)
                }
            }

            override fun onFailure(call: Call<RecipesDTO>, t: Throwable) { }
        })
        // init stuff
        viewPager = findViewById(R.id.viewPager)
        val t = arrayOf("SEARCH BY INGREDIENT", "FAVORITES")
        viewPager.adapter = SectionsPagerAdapter(supportFragmentManager, lifecycle)
        val tabLayout = findViewById<TabLayout>(R.id.tabs)

        // Set tabs text
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = t[position]
        }.attach()
        // When tab is selected, create the fragment that needs to be created and remove the
        // previous fragment
        val nested = findViewById<NestedScrollView>(R.id.nested)
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                supportFragmentManager.popBackStack()
                nested.removeAllViews()
                when(tab.position) {
                    0 ->  {
                        val s = SearchFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .add(R.id.nested, s)
                            .commit()
                    }
                    1 -> {
                        val f = FavoritesFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .add(R.id.nested, f)
                            .commit()
                    }
                }

            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }
}