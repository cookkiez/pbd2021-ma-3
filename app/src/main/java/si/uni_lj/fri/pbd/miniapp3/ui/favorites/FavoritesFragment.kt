package si.uni_lj.fri.pbd.miniapp3.ui.favorites

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.database.ViewModel
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity
import si.uni_lj.fri.pbd.miniapp3.ui.MainActivity

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FavoritesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavoritesFragment : Fragment(), RecyclerViewAdapter.OnItemClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mViewModel: ViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    // Set item clicked event for recycler view, also tell DetailsActivity, from which fragment it was created
    override fun onItemClicked(id : String) {
        val intent = Intent(context, DetailsActivity::class.java).apply {
            putExtra("id", id)
            putExtra("fragment", "favorites")
        }
        startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        val recycler = view.findViewById<RecyclerView>(R.id.recyclerView2)
        mViewModel = ViewModel(activity!!.application)
        val service = ServiceGenerator.createService(RestAPI::class.java)
        val semester = view.findViewById<Button>(R.id.semester)
        val c = context
        semester.setOnClickListener {
            if (!MainActivity.semesterMode) {
                MainActivity.semesterMode = true
                val toPrint = if (MainActivity.semesterMode) "ON" else "OFF"
                Toast.makeText(c, "Semester mode is set to : $toPrint, switch tabs and come back to update this screen", Toast.LENGTH_SHORT).show()
            }
            else {
                val builder = AlertDialog.Builder(context!!)
                val input = EditText(context!!)
                input.hint = "Enter password"
                input.inputType = InputType.TYPE_CLASS_TEXT
                builder.setView(input)
                builder.setPositiveButton("OK") { dialog, which ->
                    if (input.text.toString() == "studyhard") {
                        MainActivity.semesterMode = false
                        val toPrint = if (MainActivity.semesterMode) "ON" else "OFF"
                        Toast.makeText(c, "Semester mode is set to : $toPrint, switch tabs and come back to update this screen", Toast.LENGTH_SHORT).show()
                        dialog.cancel()
                    } else {
                        Toast.makeText(c, "Wrong password", Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton("Cancel") { dialog, which ->
                    dialog.cancel()
                }
                builder.show()
            }

        }

        Toast.makeText(context, "Semester mode is : " + if (MainActivity.semesterMode) "ON" else "OFF", Toast.LENGTH_SHORT).show()
        //Get all the favorite recipes from the database
        mViewModel!!.allRecipes.observe(viewLifecycleOwner, { recipes ->
            val newRecipes = mutableListOf<RecipeDTO>()
            var i = 0
            for (rec in recipes) {
                if ((MainActivity.semesterMode && MainActivity.nonAlcoholic.contains(rec.idDrink)) || !MainActivity.semesterMode) {
                    newRecipes.add(Mapper.mapRecipeDetailsToRecipeDTO(rec))
                }
                i++
            }
            if (newRecipes.size == 0) {
                Toast.makeText(context, "You have no favorite drinks!", Toast.LENGTH_SHORT).show()
            }
            val ad = RecyclerViewAdapter(newRecipes, this)
            recycler.adapter = ad
            recycler.layoutManager = LinearLayoutManager(context!!)
        })
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FavoritesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(semester:Boolean) =
            FavoritesFragment().apply {
                arguments = Bundle().apply {
                    putBoolean("semester", semester)
                }
            }
    }
}