package si.uni_lj.fri.pbd.miniapp3.ui.search

import android.app.Activity
import android.app.AlertDialog
import android.app.Service
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity
import si.uni_lj.fri.pbd.miniapp3.ui.MainActivity
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesFragment

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment(), RecyclerViewAdapter.OnItemClickListener {
    private var param1: String? = null
    private var param2: String? = null
    private val baseUrl: String = "https://www.thecocktaildb.com/api/json/v1/1/"
    var act = this
    var material : MaterialProgressBar? = null
    var recycler : RecyclerView? = null
    var service : RestAPI? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }
    // Set item clicked event for recycler view, also tell DetailsActivity, from which fragment it was created
    override fun onItemClicked(id : String) {
        val intent = Intent(context, DetailsActivity::class.java).apply {
            putExtra("id", id)
            putExtra("fragment", "search")
        }
        startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        // init stuff
        material = view.findViewById<MaterialProgressBar>(R.id.materialProgressBar)
        material!!.visibility = View.VISIBLE
        val spinner = view.findViewById<Spinner>(R.id.spinner)
        recycler = view.findViewById<RecyclerView>(R.id.recyclerView)
        service = ServiceGenerator.createService(RestAPI::class.java)

        val call = service!!.allIngredients
        val semester = view.findViewById<Button>(R.id.semester)
        val c = context
        semester.setOnClickListener {
            if (!MainActivity.semesterMode) {
                MainActivity.semesterMode = true
                val toPrint = if (MainActivity.semesterMode) "ON" else "OFF"
                Toast.makeText(c, "Semester mode is set to : $toPrint", Toast.LENGTH_SHORT).show()
                getByIngred("" + spinner.selectedItem)
            }
            else {
                val builder = AlertDialog.Builder(context!!)
                val input = EditText(context!!)
                input.hint = "Enter password"
                input.inputType = InputType.TYPE_CLASS_TEXT
                builder.setView(input)
                builder.setPositiveButton("OK") { dialog, which ->
                    if (input.text.toString() == "studyhard") {
                        MainActivity.semesterMode = false
                        val toPrint = if (MainActivity.semesterMode) "ON" else "OFF"
                        Toast.makeText(c, "Semester mode is set to : $toPrint", Toast.LENGTH_SHORT).show()
                        getByIngred("" + spinner.selectedItem)
                        dialog.cancel()
                    } else {
                        Toast.makeText(c, "Wrong password", Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton("Cancel") { dialog, which ->
                    dialog.cancel()
                    val toPrint = if (MainActivity.semesterMode) "ON" else "OFF"
                    Toast.makeText(c, "Semester mode is set to : $toPrint", Toast.LENGTH_SHORT)
                        .show()
                }
                builder.show()
            }

        }

        // Get data from API and store it into adapter, then set the adapter to spinner
        // If error on API call make a toast to the user
        call!!.enqueue(object : Callback<IngredientsDTO?> {
            override fun onFailure(call: Call<IngredientsDTO?>, t: Throwable) {
                Toast.makeText(context, "Could not get data about ingredients from API", Toast.LENGTH_SHORT).show()
            }
            override fun onResponse(
                call: Call<IngredientsDTO?>,
                response: Response<IngredientsDTO?>
            ) {
                if (response.code() == 200) {
                    material!!.visibility = View.INVISIBLE
                    val ingredients = response.body()
                    var arr = mutableListOf<String>()
                    for (ingr in ingredients!!.ingredients!!) {
                        arr.add(ingr.strIngredient1!!)
                    }
                    val data = SpinnerAdapter(arr)
                    spinner.adapter = data
                    spinner.setSelection(6)
                } else {
                    material!!.visibility = View.INVISIBLE
                    Toast.makeText(context, "Could not get data about ingredients from API", Toast.LENGTH_SHORT).show()
                }
            }
        })

        // When ingredient selected in spinner, get all drinks with this ingredient and store them
        // into adapter and then connect adapter to recyclerView. On API error display Toast to user
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                getByIngred(parent!!.getItemAtPosition(position).toString())
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d("Search", "Nothing selected")
            }
        }

        return view
    }

    private fun getByIngred(i : String) {
        val rec = service!!.recipesByIngredientDTO(i)
        material!!.visibility = View.VISIBLE
        rec.enqueue(object : Callback<RecipesDTO?> {
            override fun onResponse(call: Call<RecipesDTO?>, response: Response<RecipesDTO?>) {
                material!!.visibility = View.INVISIBLE
                val recipes = response.body()
                var ad : RecyclerViewAdapter? = null
                if (MainActivity.semesterMode) {
                    var newRecipes : MutableList<RecipeDTO> = mutableListOf()
                    for (rec in recipes!!.recipes!!) {
                        if (MainActivity.nonAlcoholic.contains(rec.idDrink)) {
                            newRecipes.add(rec)
                        }
                    }
                    if (newRecipes.size == 0 ) {
                        Toast.makeText(context, "There are no non-alcoholic drinks with this ingredient!", Toast.LENGTH_SHORT)
                    }
                    ad = RecyclerViewAdapter(newRecipes, act)
                } else {
                    ad = RecyclerViewAdapter(recipes!!.recipes!!, act)
                }
                recycler!!.adapter = ad
                recycler!!.layoutManager = LinearLayoutManager(context!!)
            }

            override fun onFailure(call: Call<RecipesDTO?>, t: Throwable) {
                material!!.visibility = View.INVISIBLE
                Toast.makeText(context, "Drinks with this ingredient do not exist!", Toast.LENGTH_SHORT).show()
            }

        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(semester:Boolean) =
            SearchFragment().apply {
                arguments = Bundle().apply {
                    putBoolean("semester", semester)
                }
            }
    }
}
